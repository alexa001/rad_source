﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAD_BAL.Models
{
    public class Licensee
    {
        public int ID { get; set; }
        public string LicenseeName { get; set; }
        public string LicenseNumber { get; set; }
        public DateTime ExpireDate { get; set; }
        public int UseId { get; set; }
        public string LicenseeName2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string PostalCode1 { get; set; }
        public string PostalCode2 { get; set; }
        public string EmailAddress { get; set; }
        public DateTime TerminationDate { get; set; }
        public string StreetAddress { get; set; }
    }
}
