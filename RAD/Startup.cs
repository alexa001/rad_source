﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RAD.Startup))]
namespace RAD
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
