﻿<%@ Page Title="RAD HOME" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="RAD.Home" %>

<asp:Content ID="Home" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-left: 30px; padding-top: 20px;">
        <div>
            <asp:UpdateProgress ID="prgLoadingStatus" runat="server" DynamicLayout="true">
                <ProgressTemplate>
                    <div id="overlay">
                        <div id="modalprogress">
                            <div id="theprogress">
                                <asp:Image ID="imgWaitIcon" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ajax-loader.gif" />
                                Please wait...
                            </div>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
             <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
            <div style="padding: 10px 10px 10px 50px; font-family: Arial; font-size: 12px; font-weight: bold">
                Select Type:
        <asp:DropDownList ID="ddlType" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" runat="server">
            <asp:ListItem Text="--Select Type--" Value="0"></asp:ListItem>
            <asp:ListItem Text="License" Value="1"></asp:ListItem>
            <asp:ListItem Text="Reciprocity" Value="2"></asp:ListItem>
        </asp:DropDownList>
            </div>
            <div style="padding: 10px 10px 10px 50px;">
                <asp:Panel ID="pnlLicense" Visible="false" runat="server" Width="400px">
                    <div style="height: 300px; width: 100%; padding: 10px 10px 10px 10px; border: 1px solid black">
                        <%--  --%>
                    </div>

                </asp:Panel>
                <asp:Panel ID="pnlReciprocity" Visible="false" runat="server" Width="400px">
                    <div style="height: 300px; width: 100%; border: 1px solid black">
                    </div>

                </asp:Panel>
            </div>
             </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
