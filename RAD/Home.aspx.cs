﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RAD
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //This is test new 2016
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex != 0)
            {
                if (ddlType.SelectedItem.Value == "1")
                {
                    pnlLicense.Visible = true;
                    pnlReciprocity.Visible = false;
                }
                else if (ddlType.SelectedItem.Value == "2")
                {
                    pnlLicense.Visible = false;
                    pnlReciprocity.Visible = true;
                }
            }
            else
            {
                pnlLicense.Visible = false;
                pnlReciprocity.Visible = false;
            }
        }
    }
}