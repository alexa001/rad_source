﻿function jScript() {
    $(document).ready(function () {
        //hide al div containers
        //$('.collapsible_div div').hide();
        //append click event to the a element.
        $('.collapsible_div a').click(function (e) {
            //SLide down the corresponding div if hidden, or slide up if shown
            $(this).parent().next('.collapsible_div div').slideToggle(600);
            //set the current item as active
            $(this).parent().toggleClass('active');
            e.preventDefault();
        });

    });
}